## LLVM-d

This repository was used to auto-generate [D](https://www.dlang.org) bindings to the [LLVM C API](https://llvm.org/doxygen/group__LLVMC.html).

It was initially created for [styx](https://gitlab.com/styx-lang/styx) but was never really used because the automated translation 
in CI environments, the difference between the installation path of LLVM headers depending on the linux distribution,
made the operation complicated.

Instead a specific version was generated locally, commited to a branch
(e.g [9](https://gitlab.com/basile.b/llvm-d/-/tree/llvm-9-generated), [11](https://gitlab.com/basile.b/llvm-d/-/tree/llvm-11-generated))
and styx used a submodule + a checkout.

### Installation & Usage

See previous revisions of this file.
