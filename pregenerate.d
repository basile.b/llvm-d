module pregenerate;

import std.algorithm.iteration;
import std.array;
import std.file;
import std.path;
import std.process;
import std.stdio;
import std.string;

string dstepPath()
{
    debug { writeln(__PRETTY_FUNCTION__); stdout.flush(); }
    if ("./dstep".exists())
        return "./dstep";
    if (auto s = executeShell("which dstep").output.strip)
        return s;
    if (auto s = "DSTEP" in environment)
        return environment["DSTEP"];
    assert(false, "dstep cannot be found");
}

string llvmHeaderPath()
{
    debug { writeln(__PRETTY_FUNCTION__); stdout.flush(); }
    static immutable paths = [
        "/llvm-c",
        "/usr/include/llvm-c",
        "/usr/include/llvm-c-9.0/llvm-c",
        "/usr/include/llvm-c-9/llvm-c",
        "/usr/include/llvm-c-8.0/llvm-c",
        "/usr/include/llvm-c-8/llvm-c",
        "/usr/include/llvm-c-7/llvm-c",
        "/usr/include/llvm-c-7.0/llvm-c",
        "/usr/include/llvm-c-6.0/llvm-c",
        "/usr/include/llvm-c-6/llvm-c",
    ];
    foreach (p; paths)
        if (p.exists())
            return p;
    assert(false, "LLVM header location cannot be found");
}

void copyHeaders()
{
    debug { writeln(__PRETTY_FUNCTION__); stdout.flush(); }
    const p = llvmHeaderPath();
    foreach (e; dirEntries(p, "*.h", SpanMode.depth))
    {
        const np = "llvm-c" ~ e.name[p.length .. $];
        mkdirRecurse(np.dirName());
        copy(e.name, np);
    }
}

void translate()
{
    debug { writeln(__PRETTY_FUNCTION__); stdout.flush(); }
    const h = dirEntries("llvm-c/", "*.h", SpanMode.depth).map!(a => a.name).array;
    const r = execute([dstepPath] ~ h ~ ["-o", "src", "--collision-action=ignore",
        "--normalize-modules=true", "--package=llvm", "--global-import=llvm.types"]);
    assert(r.status == 0, r.output);
}

void fixupImports()
{
    debug { writeln(__PRETTY_FUNCTION__); stdout.flush(); }
    foreach (e; dirEntries("translation-fixes", "*.patch", SpanMode.depth))
    {
        assert("src/llvm/" ~ e.name.baseName().stripExtension().exists());
        auto r = execute(["patch", "-i", e.name, "src/llvm/" ~ e.name.baseName().stripExtension()]);
        assert(r.status == 0, r.output);
    }
}

void fixCasing()
{
    debug { writeln(__PRETTY_FUNCTION__); stdout.flush(); }
    mkdir("src/llvm/transforms");
    foreach (e; dirEntries("src", "*.d", SpanMode.depth))
        rename(e.name, toLower(e.name));
    if ("src/llvm/Transforms".exists())
        rmdir("src/llvm/Transforms");
}

void main(string[] args)
{
    if ("src".exists)
    {
        writeln("llvm-d sources appear to be already generated and compiled, exiting...");
        writeln("(hint: delete src/ to trigger a rebuild)");
        return;
    }
    if (!"llvm-c".exists())
    {
        copyHeaders();
    }
    translate();
    std.stdio.rename("src/llvm-c", "src/llvm");
    fixupImports();
    fixCasing();
    return;
}
