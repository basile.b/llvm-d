/+ dub.sdl:
    name        "check_linking"
    dependency  "llvm-d"        path="../"
+/
module check_linking;

import llvm.core;

void main(string[] args)
{
    auto ctx = LLVMContextCreate();
    assert(ctx);
    scope(exit) LLVMContextDispose(ctx);
}
